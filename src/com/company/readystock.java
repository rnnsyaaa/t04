package com.company;

class readystock extends kue{
    private double jumlah;

    public readystock(String name, double price, double jumlah) {
        // untuk menginisialisasi nilai dari atribut yg ada pada objek tersebut.
        super(name, price); // untuk memanggil kontruktor dari parent class
        this.jumlah = jumlah;
    }

    public double getJumlah() {
        return jumlah; // untuk mengembalikan nilai jumlah
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public double hitungHarga() {
        return getPrice() * jumlah * 2; // digunakan untuk menghitung harga x dan x 2
    }

    @Override
    public double Berat() {
        return 0; // berat = 0 adalah untuk mengembalikan nilai karena berat tidak digunakan di sini
    }

    @Override
    public double Jumlah() {
        return jumlah;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("\nJumlah\t\t:%.2f\nTotal Harga\t:%.2f", jumlah, hitungHarga());
    // method to.String dipakai untuk mencetak informasi kue ready stock mulai dari nama kue, jumlah, dan harga
    }
}
