package com.company;

class pesanan extends kue{
private  double berat;


    public pesanan(String name, double price, double berat) {
        // untuk menginisialisasi nilai dari atribut yg ada pada objek tersebut.
        super(name, price); // untuk memanggil kontruktor dari parent class
        this.berat = berat;
    }
    public double getBerat() {
        return berat; // untuk mengembalikan nilai berat
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }
    @Override
    public double hitungHarga() {
        return getPrice() * berat; // untuk menghitung harga * berat
    }

    @Override
    public double Berat() {
        return berat; // mengembalikan nilai berat
    }

    @Override
    public double Jumlah() {
        return 0; // jumlah = 0 untuk mengembalikan nilai karena jumlah tidak di pakai pada class pesanan
    }

    @Override
    public String toString() {
        return super.toString() + String.format("\nBerat\t\t:%.2f\nTotal Harga\t:%.2f", berat, hitungHarga());
        // method to.String dipakai untuk mencetak informasi kue yg dipesan  (pesanan) mulai dari nama kue, harga, dan berat

    }

}
